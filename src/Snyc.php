<?php

namespace Llencryption;

class Snyc
{

    private $token;
    private $algorithm;
    private static $instance;

    /**
     * @return Snyc
     */
    public static function instance()
    {
        if(!self::$instance){
            self::$instance = new self();

        }

        return self::$instance;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @param mixed $algorithm
     */
    public function setAlgorithm($algorithm)
    {
        $this->algorithm = $algorithm;
    }

    /**
     * @param $string
     * @return string
     * @throws \Exception
     */
    public function encrypt($string)
    {
        $this->checkToken();

        $output = false;
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->algorithm));
        // hash
        $key = hash('sha256', $this->token);


        $output = openssl_encrypt($string, $this->algorithm, $key, 0, $iv);
        return base64_encode($output . '::' . $iv);
    }

    /**
     * @param $string
     * @return false|string
     * @throws \Exception
     */
    public function decrypt($string)
    {
        $this->checkToken();

        $key = hash('sha256', $this->token);

        list($encrypted_data, $iv) = explode('::', base64_decode($string), 2);
        return openssl_decrypt($encrypted_data, $this->algorithm, $key, 0, $iv);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function checkToken()
    {
        if (empty($this->token)) {
            throw new \Exception('CryptToken is empty');
        }

        if (empty($this->algorithm)) {
            throw new \Exception('Algorithm is empty');
        }

        return true;
    }

    /**
     * Snyc constructor.
     */
    private function __construct()
    {
    }
}

