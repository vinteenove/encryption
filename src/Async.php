<?php


namespace Llencryption;


class Async
{
    private $publicKey;
    private $privateKey;

    /**
     * @param mixed $publicKey
     */
    public function setPublicKey($publicKey): void
    {
        $this->publicKey = $publicKey;
    }

    /**
     * @param mixed $privateKey
     */
    public function setPrivateKey($privateKey): void
    {
        $this->privateKey = $privateKey;
    }


    /**
     * @param $data
     * @return string
     */
    public function publicEncrypt($data): string
    {
        openssl_public_encrypt($data, $encrypted, $this->publicKey);

        return base64_encode($encrypted);
    }
    /**
     * @param $data
     * @return string
     */
    public function privateEncrypt($data): string
    {
        openssl_private_encrypt($data,$encrypted, $this->privateKey);
        return base64_encode($encrypted);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function publicDecrypt($data)
    {
        openssl_public_decrypt(base64_decode($data), $decrypted, $this->publicKey);
        return $decrypted;
    }

    /**
     * @param $data
     * @return string
     */
    public function privateDecrypt($data): string
    {
        openssl_private_decrypt(base64_decode($data), $decrypted, $this->privateKey);
        return $decrypted;
    }

    public function __construct($privateKey = null, $publicKey = null)
    {
        if($privateKey != null){
            $this->privateKey = $privateKey;
        }

        if($publicKey != null){
            $this->publicKey = $publicKey;
        }
    }
}
