<?php


namespace Llencryption;


class Key
{
    private $pKey;
    private $privateKey;
    private $publicKey;

    /**
     * @return mixed
     */
    public function createPrivateKey()
    {
        $this->pKey = openssl_pkey_new(["digest_alg"       => "sha512",
                                        "private_key_bits" => 1024,
                                        "private_key_type" => OPENSSL_KEYTYPE_RSA]);

        if ($this->pKey === false) {
            echo openssl_error_string();
            die("\nFailed to generate key pair");
        }

        if (!openssl_pkey_export($this->pKey, $this->privateKey)) {
            echo openssl_error_string();
            die("\nFailed to retrieve private key");
        }


        return $this->privateKey;
    }

    public function createPublicKey($var)
    {
        $public_key_pem = openssl_pkey_get_details($this->pKey);
        $this->publicKey = $public_key_pem['key'];

        return $this->publicKey;
    }

//    /**
//     * @param string $emailAddress
//     * @param string $commonName
//     * @param string $organizationName
//     * @param string $countryName
//     * @param string $stateOrProvinceName
//     * @param string $localityName
//     * @return mixed
//     */
//    public function createPublicKey($emailAddress,
//                                    $commonName = "lliure",
//                                    $organizationName = "lliure",
//                                    $countryName = "BR",
//                                    $stateOrProvinceName = "São Paulo",
//                                    $localityName = "São Paulo"
//    )
//    {
//        $dn = array(
//            "countryName"            => $countryName,
//            "stateOrProvinceName"    => $stateOrProvinceName,
//            "localityName"           => $localityName,
//            "organizationName"       => $organizationName,
//            "organizationalUnitName" => $organizationName,
//            "commonName"             => $commonName,
//            "emailAddress"           => $emailAddress
//        );
//
//        $cert = openssl_csr_new($dn, $this->privateKey);
//        $cert = openssl_csr_sign($cert, null, $this->privateKey, 365);
//        openssl_x509_export($cert, $this->publicKey);
//
//        return $this->publicKey;
//    }

    /**
     * @param mixed $privateKey
     */
    public function setPrivateKey($privateKey): void
    {
        $this->privateKey = $privateKey;
        $this->pKey = openssl_pkey_get_private($privateKey);
    }
}