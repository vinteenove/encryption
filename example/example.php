<?php

require_once "../vendor/autoload.php";

//phpinfo();
//die();

$key = new \Llencryption\Key();

$keypriv = $key->createPrivateKey();
echo '<pre>' . $keypriv. '</pre>' . "\n";

$keypub = $key->createPublicKey('jfrasson@vinteenove.com.br');
echo '<pre>' . $keypub. '</pre>' . "\n";

echo "<br<br><br><br>";
$texto = "Vinte e nove é a melhor!";
echo "<pre>-----Original Text-----\n" . $texto. '</pre>' . "\n";



$cript = new \Llencryption\Async($keypriv, $keypub);

$privCript = $cript->privateEncrypt($texto);
echo "<pre>-----Private ecripted-----\n" . $privCript. '</pre>' . "\n";

$pubDecript = $cript->publicDecrypt($privCript);
echo "<pre>-----Public decripted-----\n" . $pubDecript. '</pre>' . "\n";


$publicEncrypt = $cript->publicEncrypt($texto);
echo "<pre>-----Public ecripted-----\n" . $publicEncrypt. '</pre>' . "\n";

$privDecript = $cript->privateDecrypt($publicEncrypt);
echo "<pre>-----Private decripted-----\n" . $privDecript. '</pre>' . "\n";
